﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task_23_Normal.Controllers
{
    public class HomeController : Controller
    {
        public class News
        {
            public string Name { get; set; }
            public string Text { get; set; }
            public string Date { get; set; }
        }

        private static List<News> news = CreateNews();

        public ActionResult Index()
        {
            ViewBag.News = news;
            return View();
        }

        private static List<News> CreateNews()
        {
            return new List<News>
            {
                new News { Date = "19.4.2019", Name = "First", Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a auctor massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed posuere gravida placerat. Fusce non neque ac felis facilisis rhoncus. Aliquam iaculis maximus condimentum. In vel congue arcu, vel mollis felis. In nec nisi ipsum. In laoreet imperdiet ante in iaculis. Mauris ornare, dolor vitae facilisis eleifend, quam nibh eleifend elit, mollis faucibus massa urna id lacus. Phasellus eu lorem rhoncus, aliquam neque vitae, dictum enim. Etiam sed metus ac felis placerat euismod. Donec quam lacus, ornare eu mauris quis, pharetra pellentesque tellus. Etiam eu accumsan libero." },
                new News { Date = "07.11.2020", Name = "Second", Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a auctor massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed posuere gravida placerat. Fusce non neque ac felis facilisis rhoncus. Aliquam iaculis maximus condimentum. In vel congue arcu, vel mollis felis. In nec nisi ipsum. In laoreet imperdiet ante in iaculis. Mauris ornare, dolor vitae facilisis eleifend, quam nibh eleifend elit, mollis faucibus massa urna id lacus. Phasellus eu lorem rhoncus, aliquam neque vitae, dictum enim. Etiam sed metus ac felis placerat euismod. Donec quam lacus, ornare eu mauris quis, pharetra pellentesque tellus. Etiam eu accumsan libero." },
                new News { Date = "21.3.2021", Name = "Third", Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas a auctor massa. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Sed posuere gravida placerat. Fusce non neque ac felis facilisis rhoncus. Aliquam iaculis maximus condimentum. In vel congue arcu, vel mollis felis. In nec nisi ipsum. In laoreet imperdiet ante in iaculis. Mauris ornare, dolor vitae facilisis eleifend, quam nibh eleifend elit, mollis faucibus massa urna id lacus. Phasellus eu lorem rhoncus, aliquam neque vitae, dictum enim. Etiam sed metus ac felis placerat euismod. Donec quam lacus, ornare eu mauris quis, pharetra pellentesque tellus. Etiam eu accumsan libero." }
            };
        }

    }
}