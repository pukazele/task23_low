﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Task_23_Normal.Controllers
{
    public class GuestController : Controller
    {
        public class Review
        {
            public string Author { get; set; }
            public string Text { get; set; }
            public string Date { get; set; }
        }

        private static List<Review> reviews = CreateReviews();

        public ActionResult Index()
        {
            ViewBag.Reviews = reviews;
            return View();
        }

        public ActionResult LeaveComment(string author, string text)
        {
            string date = DateTime.Now.Day.ToString() + "." + DateTime.Now.Month.ToString() + "." + DateTime.Now.Year.ToString();

            reviews.Add(new Review { Author = author, Text = text, Date = date });
            return RedirectToAction("Index");
        }

        [HttpPost]
        private static List<Review> CreateReviews()
        {
            return new List<Review>
            {
                new Review { Date = "19.4.2019", Author = "AOT", Text = "Good, kruto, norm" },
                new Review { Date = "07.11.2020", Author = "Geniy123", Text = "Ya lublu mamu" },
                new Review { Date = "21.3.2021", Author = "Zheki4", Text = "Mozhem povtorit 1941-1945!!!" }
            };
        }
    }
}